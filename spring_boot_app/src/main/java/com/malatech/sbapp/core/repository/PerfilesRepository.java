package com.malatech.sbapp.core.repository;

 
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.malatech.sbapp.core.model.Perfil;

@Repository("perfilesRepository")
public interface PerfilesRepository extends JpaRepository<Perfil, Long>{

	Perfil findByDescripcion(String descripcion);
}
