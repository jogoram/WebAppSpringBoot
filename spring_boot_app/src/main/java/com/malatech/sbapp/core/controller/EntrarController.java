package com.malatech.sbapp.core.controller;


import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.malatech.sbapp.core.model.Usuarios;
import com.malatech.sbapp.core.model.service.UsuariosService;

@Controller
public class EntrarController {
	
	@Autowired
	private UsuariosService usuariosService;

	@RequestMapping(value={"/", "/ingresar"}, method = RequestMethod.GET)
	public ModelAndView login(){
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("ingresar");
		return modelAndView;
	}
	
	
	@RequestMapping(value="/registro", method = RequestMethod.GET)
	public ModelAndView registration(){
		ModelAndView modelAndView = new ModelAndView();
		Usuarios usuario= new Usuarios();
		modelAndView.addObject("usuario", usuario);
		modelAndView.setViewName("registro");
		return modelAndView;
	}
	
	@RequestMapping(value = "/registro", method = RequestMethod.POST)
	public ModelAndView createNewUser(@Valid Usuarios usuario, BindingResult bindingResult) {
		ModelAndView modelAndView = new ModelAndView();
		System.out.println("usuario: "+usuario.getUsuario());
		System.out.println("contraseña: "+usuario.getContrasena());
		
		
		Usuarios usuarioExistente = usuariosService.buscarPorUsuario(usuario.getUsuario());

		if (usuarioExistente!= null) {
			bindingResult
					.rejectValue("usuario", "error.user",
							"El nombre de usuario ya se ha registrado");
		}
		if (bindingResult.hasErrors()) {
			modelAndView.setViewName("registro");
		} else {
			usuariosService.guardarUsuario(usuario);
			modelAndView.addObject("successMessage", "User has been registered successfully");
			modelAndView.addObject("usuario", new Usuarios());
			modelAndView.setViewName("registro");
			
		}
		return modelAndView;
	}
	
	@RequestMapping(value="/admin/inicio", method = RequestMethod.GET)
	public ModelAndView home(){
		ModelAndView modelAndView = new ModelAndView();
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Usuarios usuario= usuariosService.buscarPorUsuario(auth.getName());
		modelAndView.addObject("userName", "Welcome " + usuario.getNombre()+ " " + usuario.getApellidoPaterno()+ " (" + usuario.getUsuario() + ")");
		modelAndView.addObject("adminMessage","Content Available Only for Users with Admin Role");
		modelAndView.setViewName("admin/inicio");
		return modelAndView;
	}
	

}