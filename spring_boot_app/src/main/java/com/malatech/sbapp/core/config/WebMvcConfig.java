package com.malatech.sbapp.core.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;


@Configuration
//@PropertySource("classpath:/com/malatech/sbapp/core/config/api.properties")//swagger
public class WebMvcConfig  {

		@Bean
		public BCryptPasswordEncoder passwordEncoder() {
			BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
			return bCryptPasswordEncoder;
		}
	
	
//		@Bean
//	    public CommonsMultipartResolver multipartResolver() {
////	        return new StandardServletMultipartResolver();
//	    	CommonsMultipartResolver resolver = new CommonsMultipartResolver();
//	    	return resolver;
//	    			
//	    
//	    }
	    
	    @Bean
	    public Docket api() { 
	        return new Docket(DocumentationType.SWAGGER_2)  
	          .select()                                  
	          .apis(RequestHandlerSelectors.basePackage("com.malatech.sbapp.core.api"))              
	          .paths(PathSelectors.any())                          
	          .build()          
	          .apiInfo(apiInfo());
	    }
	    
	    ApiInfo apiInfo() {
	        return new ApiInfoBuilder()
	            .title("API sbapp")
	            .description("documentacion as")
//	            .license("Apache 2.0")
//	            .licenseUrl("http://www.apache.org/licenses/LICENSE-2.0.html")
//	            .termsOfServiceUrl("")
	            .version("1.0.0")
	            .contact(new Contact("","", "info@malatech.com"))
	            .build();
	    }
	    
	

}