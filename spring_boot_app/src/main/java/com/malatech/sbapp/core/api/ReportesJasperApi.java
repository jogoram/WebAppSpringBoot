package com.malatech.sbapp.core.api;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.malatech.sbapp.core.model.Usuarios;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-06-13T22:12:10.696Z")

@Api(value = "Jasper", description = "Reportes Jasper")
@RequestMapping(value = "/api/jasper/")
public interface ReportesJasperApi {

    
    @ApiOperation(value = "Listar reportes", notes = "", response = Usuarios.class, tags={ "reportes","jasper" })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "operacion exitosa", response = String.class),
        @ApiResponse(code = 404, message = "recurso no encontrado", response = String.class) })
    @RequestMapping(produces = { "application/json"}, 
        method = RequestMethod.GET)
    ResponseEntity<List<java.util.Map.Entry<Object,Object>>> getListaReportes();
    
    @ApiOperation(value = "Consultar reportes pdf", notes = "", response = Usuarios.class, tags={ "reportes","jasper" })
    @ApiResponses(value = { 
    		@ApiResponse(code = 200, message = "operacion exitosa", response = byte[].class),        
    		@ApiResponse(code = 404, message = "recurso no encontrado", response = byte[].class) })
    @RequestMapping(value = "pdf/{reporte}",
    produces = { "application/pdf"}, 
    method = RequestMethod.GET)
    ResponseEntity<byte[]> getReportePdf(HttpServletRequest request,@ApiParam(value = "nombre archivo de reporte",required=true ) @PathVariable("reporte") String reporte, @RequestParam Map<String, Object> queryMap);

    @ApiOperation(value = "Consultar reportes xlsx", notes = "", response = Usuarios.class, tags={ "reportes","jasper" })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "operacion exitosa", response = byte[].class),        
        @ApiResponse(code = 404, message = "recurso no encontrado", response = byte[].class) })
    @RequestMapping(value = "xlsx/{reporte}",
        produces = { "application/xlsx"}, 
        method = RequestMethod.GET)
    ResponseEntity<byte[]> getReporteXlsx(HttpServletRequest request,@ApiParam(value = "nombre archivo de reporte",required=true ) @PathVariable("reporte") String reporte, @RequestParam Map<String, Object> queryMap);


}
