package com.malatech.sbapp.core.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import com.malatech.sbapp.core.model.Usuarios;
import com.malatech.sbapp.core.repository.UsuariosRepository;

import io.swagger.annotations.ApiParam;
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-06-13T22:12:10.696Z")

@Controller
public class UsuariosApiController implements UsuariosApi {


    @Autowired 
    UsuariosRepository usuariosRepository ;
    public ResponseEntity<Usuarios> getUserByName(@ApiParam(value = "The name that needs to be fetched. Use user1 for testing. ",required=true ) @PathVariable("username") String username) {
        // do some magic!
    	
        return new ResponseEntity<Usuarios>(HttpStatus.OK);
    }


	@Override
	public ResponseEntity<Void> createUser( @ApiParam(value = "Created user object" ,required=true ) @RequestBody Usuarios usuario) {
		// TODO Auto-generated method stub
		return new ResponseEntity<Void>(HttpStatus.OK);
	}

   

}
