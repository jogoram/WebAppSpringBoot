package com.malatech.sbapp.core.model.service;

import com.malatech.sbapp.core.model.Perfil;

public interface PerfilService {

	public Perfil buscarPorPerfil(String perfil);
	
}