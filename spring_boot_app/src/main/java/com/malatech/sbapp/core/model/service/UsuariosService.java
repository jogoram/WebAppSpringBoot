package com.malatech.sbapp.core.model.service;

import com.malatech.sbapp.core.model.Usuarios;

public interface UsuariosService {

	public Usuarios buscarPorUsuario(String usuario);
	public boolean validarUsuario(Usuarios usuario);
	public void guardarUsuario(Usuarios usuario);
}