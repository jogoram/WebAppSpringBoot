package com.malatech.sbapp.core.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
	public class EnviarEmail {

    @Autowired private JavaMailSender mailSender;


    @RequestMapping(path = "/email-send", method = RequestMethod.GET)
    public String sendMail() {
        SimpleMailMessage message = new SimpleMailMessage();

        message.setText("Hello from Spring Boot Application");
        message.setTo("sistemas5@tecnocor.com");
        message.setFrom("sistemas5@tecnocor.com");

        try {
            mailSender.send(message);
            return "Email enviado!";
            
        } catch (Exception e) {
            e.printStackTrace();
            return "Error";
        }
    }

}