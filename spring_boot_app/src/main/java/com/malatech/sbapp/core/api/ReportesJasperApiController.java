package com.malatech.sbapp.core.api;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import io.swagger.annotations.ApiParam;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.JasperRunManager;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimpleXlsxReportConfiguration;



@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-06-13T22:12:10.696Z")

@Controller
public class ReportesJasperApiController implements ReportesJasperApi {

	@Autowired
	private DataSource dataSource;

	private HashMap obtenerParametros(Map<String, Object> queryMap) {
		HashMap<String, Object> params = new HashMap<String, Object>();
		Date d = new Date();
		for (String x : queryMap.keySet()) {
			try {
				params.put(x, Integer.parseInt(queryMap.get(x).toString()));
			} catch (NumberFormatException e) {
				try {
					params.put(x, java.sql.Date.valueOf(queryMap.get(x).toString()));
				} catch (IllegalArgumentException ex) {
					// params.put(x, queryMap.get(x).toString());
					if (queryMap.get(x).equals("true"))
						params.put(x, true);
					else if (queryMap.get(x).equals("false"))
						params.put(x, false);
					else
						params.put(x, queryMap.get(x).toString());

				}

			}
		}
		System.out.println("params   " + params.toString());
		return params;
	}

	@Override
	public ResponseEntity<byte[]> getReportePdf(HttpServletRequest request,
			@ApiParam(value = "nombre archivo de reporte", required = true) @PathVariable("reporte") String reporte,
			@RequestParam Map<String, Object> queryMap) {

		try {
			String ruta = this.getClass().getResource("/reportes/" + reporte + ".jasper").getPath().substring(1);
			byte[] output = JasperRunManager.runReportToPdf(ruta, obtenerParametros(queryMap),
					dataSource.getConnection());
			HttpHeaders responseHeaders = new HttpHeaders();
			responseHeaders.set("charset", "utf-8");
			responseHeaders.setContentType(MediaType.valueOf("application/pdf"));
			responseHeaders.setContentLength(output.length);
			responseHeaders.set("Content-disposition", "filename=" + reporte + queryMap.hashCode() + ".pdf");
			return new ResponseEntity<byte[]>(output, responseHeaders, HttpStatus.OK);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;

	}

	@Override
	public ResponseEntity<byte[]> getReporteXlsx(HttpServletRequest request,
			@ApiParam(value = "nombre archivo de reporte", required = true) @PathVariable("reporte") String reporte,
			@RequestParam Map<String, Object> queryMap) {

		try {
			String ruta = this.getClass().getResource("/reportes/" + reporte + ".jasper").getPath().substring(1);
			JasperPrint jasperPrint = JasperFillManager.fillReport(ruta, obtenerParametros(queryMap),
					dataSource.getConnection());
			JRXlsxExporter exporter = new JRXlsxExporter();
			exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
			ByteArrayOutputStream arrayOutputStream = new ByteArrayOutputStream();
			exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(arrayOutputStream));
			SimpleXlsxReportConfiguration configuration = new SimpleXlsxReportConfiguration();
			configuration.setOnePagePerSheet(true);
			configuration.setWhitePageBackground(false);
			exporter.setConfiguration(configuration);
			exporter.exportReport();
			byte[] output = arrayOutputStream.toByteArray();
			HttpHeaders responseHeaders = new HttpHeaders();
			responseHeaders.set("charset", "utf-8");
			responseHeaders.setContentType(MediaType.valueOf("application/xlsx"));
			responseHeaders.setContentLength(output.length);
			// responseHeaders.set("Content-disposition","attachment;filename="
			// + reporte + queryMap.hashCode() + ".xlsx");
			responseHeaders.set("Content-disposition", "filename=" + reporte + queryMap.hashCode() + ".xlsx");
			return new ResponseEntity<byte[]>(output, responseHeaders, HttpStatus.OK);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public ResponseEntity<List<java.util.Map.Entry<Object,Object>>> getListaReportes() {
		// TODO Auto-generated method stub
		String ruta = this.getClass().getResource("/reportes/").getPath().substring(1);
		File dir = new File(ruta);
		java.util.List<java.util.Map.Entry<Object,Object>> pairList= new java.util.ArrayList<>();		
		for (File f : dir.listFiles()) {
			if(!f.getName().endsWith(".jasper"))break;
			System.out.println(f.getName());
			try {
				JasperReport jasperReport = (JasperReport) JRLoader.loadObject(f);				
				JRParameter[] params = jasperReport.getParameters();
				List <JRParameter> paramList=new ArrayList<JRParameter>();
				for (JRParameter param : params) {
					if (!param.isSystemDefined() && param.isForPrompting()) {
						System.out.println(param.getName());			
						System.out.println(param.getDescription());			
						System.out.println(param.getDefaultValueExpression());			
						System.out.println(param.getValueClassName());		
						paramList.add(param);					
					}
				}
				Entry<Object,Object> pair1=new SimpleEntry<>(f.getName(),paramList);
				pairList.add(pair1);
			} catch (JRException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				
			}
		}
		return new ResponseEntity<List<java.util.Map.Entry<Object,Object>>>(pairList, HttpStatus.OK);		
	}
	
}
