package com.malatech.sbapp.core.api;

import javax.validation.constraints.NotNull;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.malatech.sbapp.core.model.Usuarios;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-06-13T22:12:10.696Z")

@Api(value = "usuario", description = "API de usuario")
@RequestMapping(value = "/api/usuarios/")
public interface UsuariosApi {

    @ApiOperation(value = "Crear usuario", notes = "cear un usuario nuevo", response = Void.class, tags={ "usuarios", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "Operacion exitosa", response = Void.class) })
    @RequestMapping(
        produces = { "application/xml", "application/json" }, 
        method = RequestMethod.POST)
    ResponseEntity<Void> createUser(@ApiParam(value = "Objeto usuario" ,required=true ) @RequestBody Usuarios usuario);

//
//    @ApiOperation(value = "Delete user", notes = "This can only be done by the logged in user.", response = Void.class, tags={ "user", })
//    @ApiResponses(value = { 
//        @ApiResponse(code = 400, message = "Invalid username supplied", response = Void.class),
//        @ApiResponse(code = 404, message = "User not found", response = Void.class) })
//    @RequestMapping(value = "/user/{username}",
//        produces = { "application/xml", "application/json" }, 
//        method = RequestMethod.DELETE)
//    ResponseEntity<Void> deleteUser(@ApiParam(value = "The name that needs to be deleted",required=true ) @PathVariable("username") String username);


    @ApiOperation(value = "Obtener usuario por nombre de usuario", notes = "", response = Usuarios.class, tags={ "usuarios", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "successful operation", response = Usuarios.class),
        @ApiResponse(code = 400, message = "Invalid username supplied", response = Usuarios.class),
        @ApiResponse(code = 404, message = "User not found", response = Usuarios.class) })
    @RequestMapping(value = "{username}",
        produces = { "application/xml", "application/json" }, 
        method = RequestMethod.GET)
    ResponseEntity<Usuarios> getUserByName(@ApiParam(value = "nombre de usuario ",required=true ) @PathVariable("username") String username);


//    @ApiOperation(value = "Logs user into the system", notes = "", response = String.class, tags={ "user", })
//    @ApiResponses(value = { 
//        @ApiResponse(code = 200, message = "successful operation", response = String.class),
//        @ApiResponse(code = 400, message = "Invalid username/password supplied", response = String.class) })
//    @RequestMapping(value = "/user/login",
//        produces = { "application/xml", "application/json" }, 
//        method = RequestMethod.GET)
//    ResponseEntity<String> loginUser( @NotNull @ApiParam(value = "The user name for login", required = true) @RequestParam(value = "username", required = true) String username,
//         @NotNull @ApiParam(value = "The password for login in clear text", required = true) @RequestParam(value = "password", required = true) String password);
//
//
//    @ApiOperation(value = "Logs out current logged in user session", notes = "", response = Void.class, tags={ "user", })
//    @ApiResponses(value = { 
//        @ApiResponse(code = 200, message = "successful operation", response = Void.class) })
//    @RequestMapping(value = "/user/logout",
//        produces = { "application/xml", "application/json" }, 
//        method = RequestMethod.GET)
//    ResponseEntity<Void> logoutUser();
//
//
//    @ApiOperation(value = "Updated user", notes = "This can only be done by the logged in user.", response = Void.class, tags={ "user", })
//    @ApiResponses(value = { 
//        @ApiResponse(code = 400, message = "Invalid user supplied", response = Void.class),
//        @ApiResponse(code = 404, message = "User not found", response = Void.class) })
//    @RequestMapping(value = "/user/{username}",
//        produces = { "application/xml", "application/json" }, 
//        method = RequestMethod.PUT)
//    ResponseEntity<Void> updateUser(@ApiParam(value = "name that need to be updated",required=true ) @PathVariable("username") String username,
//        @ApiParam(value = "Updated user object" ,required=true ) @RequestBody Body5 body);

}
