package com.malatech.sbapp.core.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.malatech.sbapp.core.model.Usuarios;

@Repository("usuariosRepository")
public interface UsuariosRepository extends JpaRepository<Usuarios, Integer> {
	Usuarios findByUsuario(String usuario);
}