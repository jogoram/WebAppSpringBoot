package com.malatech.sbapp.core.model.service.impl;

import java.util.Arrays;
import java.util.HashSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.malatech.sbapp.core.model.Perfil;
import com.malatech.sbapp.core.model.Usuarios;
import com.malatech.sbapp.core.model.service.UsuariosService;
import com.malatech.sbapp.core.repository.PerfilesRepository;
import com.malatech.sbapp.core.repository.UsuariosRepository;

@Service("usuariosService")
public class UsuariosServiceImpl implements UsuariosService {

	@Autowired 
	UsuariosRepository usuariosRepository; 
	@Autowired 
	PerfilesRepository  perfilesRepository;
	@Autowired
	BCryptPasswordEncoder bCryptPasswordEncoder;
	@Override
	public Usuarios buscarPorUsuario(String usuario) {
		
		return usuariosRepository.findByUsuario(usuario);
	}

	@Override
	public void guardarUsuario(Usuarios usuario) {

		
		usuario.setContrasena(bCryptPasswordEncoder.encode(usuario.getContrasena()));		
        usuario.setEstatus(1);
        Perfil perfil= perfilesRepository.findByDescripcion("ADMIN");        
        usuario.setPerfiles(new HashSet<Perfil>(Arrays.asList(perfil)));		
		usuariosRepository.save(usuario);

	}

	@Override
	public boolean validarUsuario(Usuarios usuario) {
		// TODO Auto-generated method stub
		Usuarios usuarioExistente = usuariosRepository.findByUsuario(usuario.getUsuario());
		if (usuarioExistente !=null )
		{
			if (bCryptPasswordEncoder.encode(usuario.getContrasena()).equals(usuarioExistente.getContrasena()))
				return true;		
			
			
		}
		return false;
	}

}
